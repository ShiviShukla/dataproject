import unittest
import extra_run


class TestExtraRun(unittest.TestCase):

    def test_extra_runs_conceded_by_each_team_in_2016(self):
        mock_matches_file_path = './data/mock_matches.csv'
        mock_deliveries_file_path = './data/mock_deliveries.csv'

        actual_data = extra_run.extra_runs_conceded_by_each_team_in_2016(mock_matches_file_path, mock_deliveries_file_path)
        expected_data = {'Rising Pune Supergiants': 13}

        print(actual_data)
        self.assertEqual(actual_data, expected_data)


if __name__ == '__main__':
    unittest.main()
