import csv
import sql_queries
from matplotlib import pyplot as plt


def matches_won_in_ipl(matches_file_path):

    match_winner = {}
    with open(matches_file_path) as csvfile:
        reader = csv.DictReader(csvfile)

        for row in reader:
            year = int(row['season'])
            winner = row['winner']
            if year in match_winner:
                if winner in match_winner[year] and winner:
                    match_winner[year][winner]+=1
                elif winner:
                    match_winner[year][winner] = 1
            else:
                match_winner[year] = {winner: 1}
    return match_winner


def plot_matches_won_in_ipl(data):

    ipl_year = sorted(data.keys())
    all_teams = []

    for team_win in data.values():
        all_teams.extend(team_win.keys())

    all_teams = ((set(all_teams)))

    team_all_wins = []
    ind_year = range(len(ipl_year))
    for team in all_teams:
        team_wins = []
        for year in ipl_year:
            if team in data[year].keys():
                team_wins.append(data[year][team])
            else:
                team_wins.append(0)

        team_all_wins.append({team:team_wins})

    color = {
        'Chennai Super Kings': 'aqua',
        'Deccan Chargers':'chartreuse',
        'Delhi Daredevils': 'gray',
        'Gujarat Lions': 'yellow',
        'Kings XI Punjab': 'red',
        'Kochi Tuskers Kerala': 'coral',
        'Kolkata Knight Riders': 'fuchsia',
        'Mumbai Indians': 'gold',
        'Pune Warriors': 'green',
        'Rajasthan Royals': 'lime',
        'Rising Pune Supergiant':'lavender',
        'Rising Pune Supergiants': 'teal',
        'Royal Challengers Bangalore': 'tan',
        'Sunrisers Hyderabad': 'salmon'}

    bottom_flag = False
    bar2 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for item in team_all_wins:

        for key, val in item.items():
            if bottom_flag:
                bottom = list(map(lambda x, y: x + y, bar1, bar2))
                bar2 = bottom
                plt.bar(ind_year, val, bottom=bottom,color = color[key], width=0.5)
            else:
                plt.bar(ind_year, val, color=color[key], width=0.5)

            bottom_flag = True
            bar1 = val

    plt.legend(color, loc='upper center', ncol=5, bbox_to_anchor= (0.5, 1.15))
    plt.xticks(ind_year, ipl_year)
    plt.show()


def calculate_and_plot_matches_won_in_ipl(matches_file_path):
    data = matches_won_in_ipl(matches_file_path)
    plot_matches_won_in_ipl(data)

def sql_calculate_and_plot_matches_won_in_ipl():
    data = sql_queries.matches_won()
    plot_matches_won_in_ipl(data)

if __name__ == '__main__':
    # matches_file_path = './data/matches.csv'
    # calculate_and_plot_matches_won_in_ipl(matches_file_path)
    sql_calculate_and_plot_matches_won_in_ipl()
