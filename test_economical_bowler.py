import unittest
import economical_bowler


class TestEconomicalBowler(unittest.TestCase):

    def test_top_economical_bowlers(self):
        mock_matches_file_path = './data/mock_matches.csv'
        mock_deliveries_file_path = './data/mock_deliveries.csv'
        actual_data = economical_bowler.top_economical_bowlers(mock_matches_file_path, mock_deliveries_file_path)
        expected_data = {'AD Russell': 7.0,
                         'M Morkel': 4.5,
                         'PP Chawla': 8.0,
                         'SP Narine': 7.0,
                         'Shakib Al Hasan': 11.08}

        print(actual_data)
        self.assertEqual(actual_data, expected_data)

if __name__ == '__main__':
    unittest.main()
