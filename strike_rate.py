import csv
import sql_queries
from matplotlib import pyplot as plt
from textwrap import wrap


def top_five_srike_rate(deliveries_file_path):

    batsman_run_ball = {}
    batsman_strike_rate = {}
    top_five_srike_rate_dict = {}

    with open(deliveries_file_path) as csvfile:
        reader = csv.DictReader(csvfile)

        for row in reader:
            batsman = row['batsman']
            run = int(row['batsman_runs'])
            
            if batsman in batsman_run_ball:
                batsman_run_ball[batsman]['run'] += run
                batsman_run_ball[batsman]['ball'] += 1
            else:
                batsman_run_ball[batsman] = {'run': run, 'ball': 1}

    for batsman, value in batsman_run_ball.items():
        batsman_strike_rate.update({batsman : round((value['run']/value['ball'])*100, 2)})

    top_five_srike_rate = (sorted(batsman_strike_rate.items(),key=lambda x: (x[1], x[0]), reverse=True))[:5]

    for item in top_five_srike_rate:
        batsman = item[0]
        strike_rate = item[1]
        top_five_srike_rate_dict[batsman] = strike_rate

    return top_five_srike_rate_dict


def plot_top_five_srike_rate(data):
    x_axis, y_axis = [], []
    for key in sorted(data):
        x_axis.append(key)
        y_axis.append(data[key])

    x_axis_list = range(len(x_axis))
    plt.bar(x_axis_list, y_axis)

    plt.xlabel('Bowler', color='g')
    plt.ylabel('Strike Rate', color='c')

    team = ['\n'.join(wrap(str(name), 10)) for name in x_axis]
    plt.xticks(x_axis_list, team)
    plt.title('Top Strike rate in IPL-Season')
    plt.show()


def calculate_and_plot_top_five_srike_rate(deliveries_file_path):
    data = top_five_srike_rate(deliveries_file_path)
    plot_top_five_srike_rate(data)

def sql_calculate_and_plot_top_five_srike_rate():
    data = sql_queries.strike_rate()
    plot_top_five_srike_rate(data)

if __name__ == '__main__':

    # deliveries_file_path = './data/deliveries.csv'
    # calculate_and_plot_top_five_srike_rate(deliveries_file_path)
    sql_calculate_and_plot_top_five_srike_rate()