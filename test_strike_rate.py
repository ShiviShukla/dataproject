import unittest
import strike_rate


class TestStrikeRate(unittest.TestCase):

    def test_top_five_srike_rate(self):
        mock_deliveries_file_path = './data/mock_deliveries.csv'
        actual_data = strike_rate.top_five_srike_rate(mock_deliveries_file_path)
        expected_data = {'CJ Anderson': 134.15,
                         'Harbhajan Singh': 145.16,
                         'MJ McClenaghan': 100.0,
                         'R Vinay Kumar': 109.09,
                         'RG Sharma': 147.89}

        self.assertEqual(actual_data, expected_data)

if __name__ == '__main__':
    unittest.main()
