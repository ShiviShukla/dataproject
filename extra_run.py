import csv
import sql_queries
from matplotlib import pyplot as plt
from textwrap import wrap


def extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path):

    extra_run_in_2016 = {}
    id_2016_list = []

    with open(matches_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            id = int(row['id'])
            year = int(row['season'])

            if year == 2016:
                id_2016_list.append(int(id))

    with open(deliveries_file_path) as csvfile:
        reader = csv.DictReader(csvfile)

        for row in reader:
            match_id = int(row['match_id'])

            if match_id in id_2016_list:
                bowling_team = row['bowling_team']
                extra_run = int(row['extra_runs'])

                if bowling_team in extra_run_in_2016 and bowling_team:
                    extra_run_in_2016[bowling_team] += extra_run
                elif bowling_team:
                    extra_run_in_2016[bowling_team] = extra_run
    print(extra_run_in_2016)
    return extra_run_in_2016

def plot_extra_runs_conceded_by_each_team_in_2016(data):

    x_axis, y_axis = [], []
    for key in sorted(data):
        x_axis.append(key)
        y_axis.append(data[key])

    x_axis_list = range(len(x_axis))
    plt.bar(x_axis_list, y_axis)

    plt.xlabel('Team', color='g')
    plt.ylabel('number', color='c')

    team = ['\n'.join(wrap(str(name), 10)) for name in x_axis]
    plt.xticks(x_axis_list, team)
    plt.title('Extra Run\'s in IPL-Season')
    plt.show()


def calculate_and_plot_extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path):
    result = extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path)
    plot_extra_runs_conceded_by_each_team_in_2016(result)

def sql_calculate_and_plot_extra_runs_conceded_by_each_team_in_2016():
    data = sql_queries.extra_run()
    plot_extra_runs_conceded_by_each_team_in_2016(data)

if __name__ == '__main__':
    # matches_file_path = './data/matches.csv'
    # deliveries_file_path = './data/deliveries.csv'
    # calculate_and_plot_extra_runs_conceded_by_each_team_in_2016(matches_file_path, deliveries_file_path)

    sql_calculate_and_plot_extra_runs_conceded_by_each_team_in_2016()



