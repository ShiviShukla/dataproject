import unittest
import matches_won


class TestMatchesWon(unittest.TestCase):

    def test_matches_played_per_year(self):
        mock_matches_file_path = './data/mock_matches.csv'
        actual_data = matches_won.matches_won_in_ipl(mock_matches_file_path)
        expected_data = {2017: {'Sunrisers Hyderabad': 4, 'Rising Pune Supergiant': 4, 'Kolkata Knight Riders': 6,
                                'Kings XI Punjab': 3, 'Royal Challengers Bangalore': 2, 'Mumbai Indians': 6,
                                'Delhi Daredevils': 2, 'Gujarat Lions': 3},
                         2015: {'Kolkata Knight Riders': 1},
                         2016: {'Rising Pune Supergiants': 1}}


        print(actual_data)

        self.assertEqual(actual_data, expected_data)


if __name__ == '__main__':
    unittest.main()
"""
 season |           winner            | count 
--------+-----------------------------+-------
   2016 | Rising Pune Supergiants     |     1
   2017 | Delhi Daredevils            |     2
   2017 | Sunrisers Hyderabad         |     4
   2017 | Kings XI Punjab             |     3
   2017 | Gujarat Lions               |     3
   2017 | Rising Pune Supergiant      |     4
   2017 | Royal Challengers Bangalore |     2
   2017 | Mumbai Indians              |     6
   2015 | Kolkata Knight Riders       |     1
   2017 | Kolkata Knight Riders       |     6

"""