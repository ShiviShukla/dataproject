import csv
import sql_queries
from matplotlib import pyplot as plt
from textwrap import wrap


def matches_played_per_year(matches_file_path):

    match_per_year = {}
    with open(matches_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:

            year = int(row['season'])

            if year in match_per_year and year:
                match_per_year[year] += 1
            else:
                match_per_year[year] = 1

    return match_per_year


def plot_matches_played_per_year(data):

    x_axis, y_axis = [], []
    for key in sorted(data):
        x_axis.append(key)
        y_axis.append(data[key])

    x_axis_list = range(len(x_axis))
    plt.bar(x_axis_list, y_axis)

    plt.xlabel('year', color='g')
    plt.ylabel('number', color='c')

    team = ['\n'.join(wrap(str(name), 10)) for name in x_axis]
    plt.xticks(x_axis_list, team)
    plt.title('Matches in IPL-Season')
    plt.show()


def calculate_and_plot_matches_played_per_year(matches_file_path):
    data = matches_played_per_year(matches_file_path)
    plot_matches_played_per_year(data)

def sql_calculate_and_plot_matches_played_per_year():
    data = sql_queries.matches_per_year()
    plot_matches_played_per_year(data)


if __name__ == '__main__':
    # matches_file_path = './data/matches.csv'
    # calculate_and_plot_matches_played_per_year(matches_file_path)
    sql_calculate_and_plot_matches_played_per_year()
