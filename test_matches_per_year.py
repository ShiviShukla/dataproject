import unittest
import matches_per_year

class TestMatchesPerYear(unittest.TestCase):

    def test_matches_played_per_year(self):

        mock_matches_file_path = './data/mock_matches.csv'
        actual_data = matches_per_year.matches_played_per_year(mock_matches_file_path)
        expected_data = {2017: 30, 2015: 1, 2016: 1}

        self.assertEqual(actual_data,expected_data)


if __name__ == '__main__':
    unittest.main()
