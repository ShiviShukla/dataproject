import operator
import csv
import sql_queries
from matplotlib import pyplot as plt
from textwrap import wrap


def top_economical_bowlers(matches_file_path, deliveries_file_path):
    id_2015_list = []
    bowler_economy_data = {}
    bowler_economy = {}
    top_economy = {}

    with open(matches_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            id = int(row['id'])
            year = int(row['season'])
            if year == 2015:
                id_2015_list.append(int(id))

    with open(deliveries_file_path) as csvfile:
        reader = csv.DictReader(csvfile)
        for i in reader:

            match_id = int(i['match_id'])
            if match_id in id_2015_list:
                bowler = i['bowler']
                total_run = int(i['total_runs'])

                if bowler in bowler_economy_data and bowler:
                    bowler_economy_data[bowler]['run'] += total_run
                    bowler_economy_data[bowler]['bowl'] += 1
                elif bowler:
                    bowler_economy_data[bowler] = {'run': total_run, 'bowl': 1}

    for key in bowler_economy_data:
        economy = round((bowler_economy_data[key]['run']/bowler_economy_data[key]['bowl'])*6, 2)
        bowler_economy[key] = economy

    economy = sorted(bowler_economy.items(), key=operator.itemgetter(1))
    for item in economy[0:5]:
        top_economy[item[0]] = item[1]
    return top_economy



def plot_top_economical_bowlers(data):
    x_axis, y_axis = [], []
    for key in sorted(data):
        x_axis.append(key)
        y_axis.append(data[key])

    x_axis_list = range(len(x_axis))
    plt.bar(x_axis_list, y_axis)

    plt.xlabel('Bowler', color='g')
    plt.ylabel('Economy', color='c')

    team = ['\n'.join(wrap(str(name), 10)) for name in x_axis]
    plt.xticks(x_axis_list, team)
    plt.title('Top-Economy in IPL-Season')
    plt.show()


def calculate_and_plot_top_economical_bowlers(matches_file_path, deliveries_file_path):
    data = top_economical_bowlers(matches_file_path, deliveries_file_path)
    plot_top_economical_bowlers(data)

def sql_calculate_and_plot_top_economical_bowlers():
    data = sql_queries.top_economical_bowlers()
    plot_top_economical_bowlers(data)

if __name__ == '__main__':
    # matches_file_path = './data/matches.csv'
    # deliveries_file_path = './data/deliveries.csv'
    # calculate_and_plot_top_economical_bowlers(matches_file_path, deliveries_file_path)
    sql_calculate_and_plot_top_economical_bowlers()
