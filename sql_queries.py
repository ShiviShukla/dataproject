import psycopg2


def connect_execute(query):
    con = psycopg2.connect(database="ipl_project", user="postgres", password="heycv007@", host="127.0.0.1", port="5432")
    cur = con.cursor()

    cur.execute(query)
    con.commit()
    query_output = cur.fetchall()
    con.close()

    return query_output


def matches_per_year():
    matches_played = {}
    query_output = connect_execute(query='SELECT season,COUNT(*) AS "Number" FROM ipl_data GROUP BY season;')

    for data in query_output:
        year = data[0]
        matches = data[1]
        matches_played[year] = matches
    return matches_played


def matches_won():
    match_winners = {}
    query_output = connect_execute(query='select season, winner, count(*) from ipl_data group by winner, season;')

    for data in query_output:
        year = data[0]
        team = data[1]
        won_numbers = data[2]

        if year in match_winners:
            if team in match_winners[year] and team:
                match_winners[year][team] += won_numbers
            elif team:
                match_winners[year][team] = won_numbers
                pass
        else:
            match_winners[year] = {team: won_numbers}

    return match_winners


def extra_run():
    teams_extra_runs = {}
    query_output = connect_execute(query='select bowling_team, sum(extra_runs) from ipl_data inner join '
                                         'ipl_data_deliveries on ipl_data.id = ipl_data_deliveries.match_id '
                                         'where ipl_data.season=2016 group by bowling_team;')

    for data in query_output:
        team = data[0]
        extra_runs = data[1]
        teams_extra_runs[team] = extra_runs

    return teams_extra_runs


def top_economical_bowlers():
    bowler_economy = {}
    top_five_bowler_economy = {}
    query_output = connect_execute(query='select bowler, sum(total_runs), count(bowler) from ipl_data_deliveries '
                                         'inner join ipl_data on ipl_data_deliveries.match_id = ipl_data.id'
                                         ' where ipl_data.season=2015 group by bowler;')

    for data in query_output:
        bowler = data[0]
        runs = data[1]
        ball = data[2]

        bowler_economy[bowler] = round((runs / ball) * 6, 2)

    top_bowler_economy = (sorted(bowler_economy.items(), key=lambda x: (x[1], x[0])))[:5]

    for item in top_bowler_economy[:5]:
        bowler = item[0]
        economy = item[1]
        top_five_bowler_economy[bowler] = economy

    return top_five_bowler_economy


def strike_rate():
    batsman_strike_rate = {}
    top_five_batsman_strike_rate = {}
    query_output = connect_execute(query='select batsman, sum(batsman_runs), count(batsman)'
                                         ' from ipl_data_deliveries group by batsman;')

    for data in query_output:
        batsman = data[0]
        runs = data[1]
        ball = data[2]
        batsman_strike_rate[batsman] = round((runs / ball) * 100, 2)

    top_batsman_strike_rate = (sorted(batsman_strike_rate.items(), key=lambda x: (x[1], x[0]), reverse=True))[:5]

    for item in top_batsman_strike_rate[:5]:
        batsman = item[0]
        strike_rate = item[1]
        top_five_batsman_strike_rate[batsman] = strike_rate

    return top_five_batsman_strike_rate


if __name__ == '__main__':
    pass
    # matches_file_path = "'/home/shivi/assign-project/DataProject/data/matches.csv'"
    # deliveries_file_path ="'/home/shivi/assign-project/DataProject/data/deliveries.csv'"
    # populate_matches_data(matches_file_path)
    # populate_deliveries_data(deliveries_file_path)
